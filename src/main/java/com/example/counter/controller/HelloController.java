package com.example.counter.controller;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Java男朋友
 * @date 2022-01-01 12:01
 */
@RestController
public class HelloController {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @GetMapping("/hello")
    public String hello(){
        Long views = stringRedisTemplate.opsForValue().increment("views");
        return "hello,views:" + views;
    }

}
